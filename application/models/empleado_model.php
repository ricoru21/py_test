<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class empleado_model extends CI_Model {

	
	function __construct() {
		parent::__construct();
	}

	function insert($data){
		if ($this->db->insert('empleado',$data)){
			return true;
		}else{
			return false;
		}
	}

	function update($id,$data){
		$this->db->where('idempleado',$id);
		if ($this->db->update('empleado',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function get_empleado_all(){
		$query = $this->db->query("select emp.*,c.nombre as nom_cargo, p.nombre as nom_planta, a.nombre as nom_area, p.idplanta  from empleado emp inner join cargo c on c.idcargo = emp.idcargo inner join area a on a.idarea = emp.idarea inner join planta p on p.idplanta = a.idplanta");
        return $query->result_array();
	}

	function obtenerkeyCode(){
		$query = $this->db->query("SELECT ( case CAST(emp.keycode as UNSIGNED) when 0 then '0001' else convert( right(concat('0000',( cast(ifnull(emp.keycode,0) as UNSIGNED)  + 1)), 4) using latin1) end ) as keycode_id FROM empleado emp order by idempleado desc limit 0,1");
		return $query->row();
	}
	
	function obtenerEmpleado_keyCode($keycode){
		$query = $this->db->query("select emp.*,c.nombre as nom_cargo, CONCAT(p.nombre,' / ',a.nombre) as planta_area, p.idplanta  from empleado emp inner join cargo c on c.idcargo = emp.idcargo inner join area a on a.idarea = emp.idarea inner join planta p on p.idplanta = a.idplanta where emp.keycode =".$keycode);
        return $query->row();
	}

	function get_empleados_asignar_byTipoAsignacion($tipo){
		$query = $this->db->query("select emp.idempleado, upper(CONCAT(emp.nombre,' ',emp.apellidos)) as empleado, dni from empleado emp inner join usuario u on u.idempleado = emp.idempleado where u.type =".$tipo);
        return $query->result_array();
	}

}