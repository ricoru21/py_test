<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//if (!$this->ion_auth->logged_in())
		//redirect('home', 'refresh');
	}

	public function registrar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('usuario_model','umodel');

			$username = $form["username-usuario"];
			$password = $form["password-usuario"];
			$type = $form["type-usuario"];
			$empleado = $form["empleado-usuario"];

			$data = array('username' =>$username, 
						  'password' =>sha1($password),
						  'type' =>$type,
						  'estado' =>'1',
						  'idempleado' =>$empleado,
						  );
			
			if($this->umodel->insert($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			};

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('usuario_model','umodel');

			$id = $form["id-usuario"];
			$username = $form["username-usuario"];
			$password = $form["password-usuario"];
			$type = $form["type-usuario"];
			$empleado = $form["empleado-usuario"];

			if($password!=null || $password!=""){
				$data = array('username' =>$username, 
						  'password' =>sha1($password),
						  'type' =>$type,
						  'estado' =>'1',
						  'idempleado' =>$empleado,
						  );
			}else{
				$data = array('username' =>$username,
						  'type' =>$type,
						  'estado' =>'1',
						  'idempleado' =>$empleado,
						  );
			}
			
			if($this->umodel->update($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar_perfil(){
		$form = $this->input->post('formulario');

		if ($form!=null){

			$this->load->model('usuario_model','mod');

			$lista = $form['datos'];
			$usuario = $form['usuario'];

			if ($lista!=null){			
				$listaa = array();
				foreach($lista as $key => $row){
					$list_ind = array(
						'Usuario' => intval($usuario),
						'Opcion' => intval($row["id"]),
						'estado' => $row["estado"]
					);
					array_push($listaa,$list_ind);
				}

				$this->mod->update_perfil($listaa,$usuario);
				$return = array("responseCode"=>200, "datos"=>"ok");

			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}

		}
			else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 

		$return = json_encode($return);
		echo $return;
	}

	public function get_accesos_byperfil($id_usuario)
	{	
		$this->load->model('usuario_model','mod');
		$result = $this->mod->get_accesos_byperfil($id_usuario);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

}