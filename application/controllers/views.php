<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class views extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//if (!$this->ion_auth->logged_in())
			//redirect('home', 'refresh');
	}

	public function empleado()
	{		
		$dataheader['title'] = 'Empleado';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('empleado');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/empleado.js';
		$datafooter['active'] = 'menu-empleado';
		$datafooter['dropactive'] = 'submenu-red';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

	public function configuracion()
	{		
		$dataheader['title'] = 'Usuarios';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('configuracion');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/configuracion.js';
		$datafooter['active'] = 'menu-configuracion';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

	public function usuario()
	{		
		$dataheader['title'] = 'Usuarios';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('usuario');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/usuario.js';
		$datafooter['active'] = 'menu-usuario';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

	public function reporte()
	{		
		$dataheader['title'] = 'Servicios';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('reporte');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/reporte.js';
		$datafooter['active'] = 'menu-reporte';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

	public function incidencias()
	{		
		$dataheader['title'] = 'Asistencia';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('incidencia');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/incidencia.js';
		$datafooter['active'] = 'menu-incidencia';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

}