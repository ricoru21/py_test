<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class exportarExcel extends CI_Controller {

	public function exportar() {
		parent::__construct();
	}

	function index(){
	}

	/*-- SECCION EXCEL --*/
	function toExcel_listaUsuarios() {
    $this->load->model('usuario_model','umodel');
		$data["lstUsuarios"] =$this->umodel->get_usuario_all();
		$this->load->view('reporte/reporteUsuario',$data);
	}

}
