$(document).ready(function(){
	$("#ChangePasswordForm").validationEngine('attach',{autoHidePrompt:true,autoHideDelay:3000});
	var successPassword = function(){  
		location.reload();	
	}

	var errorPassword = function(){  
		location.reload();
	}	
	
	$("#btn-guardar-contrasenia").click(function(event){
		event.preventDefault();
		if($("#ChangePasswordForm").validationEngine("validate"))
			$.blockUI({ 
				onBlock: function()
				{
					enviar($("#ChangePasswordForm").attr("action-1"),{formulario:$("#ChangePasswordForm").serializeObject()},
					 successPassword, errorPassword);
				}
		});
	});
});